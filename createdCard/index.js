const moment = require('moment');   
const ddb = require('aws-sdk/clients/dynamodb');
const dynamodb = new ddb({region: 'us-east-1'});

const generateNumber = () => {
    const min = 0000;
    const max = 9999;
    return `${Math.floor(Math.random()*(max-min+1)+min)} - ${Math.floor(Math.random()*(max-min+1)+min)} - 
            ${Math.floor(Math.random()*(max-min+1)+min)} - ${Math.floor(Math.random()*(max-min+1)+min)}`
}
exports.handler = async (event)=>{
    const sqs = event.Records.map((record) => record.body);
    for (const item of sqs) {
        const message = JSON.parse(item);
        const body = JSON.parse(message.Message);
        const age = moment().diff(moment(body.birth),'years');
        const creditCard = {
            number : generateNumber(),
            expiration : `${moment().add(4, 'years').format('MM-YY')}`,
            securityCode : `${Math.floor(Math.random()*(999-000+1)+000)}`,
            type:  age < 45 ? 'Classic' : 'Gold'
        };
        const paramsUpdate = {
            ExpressionAttributeNames: {
                "#C": "creditCard",
            },
            ExpressionAttributeValues: {
                ":c": {
                  M: {
                    "number": {
                      S: creditCard.number,
                    },
                    "expiration": {
                      S: creditCard.expiration,
                    },
                    "ccv": {
                      S: creditCard.securityCode,
                    },
                    "type":{
                      S: creditCard.type
                    }
                  },
                },
              }, 
               Key: {
                "dni": {
                  S: body.dni
                 }
               }, 
               ReturnValues: "ALL_NEW",
                TableName: "clients",
                UpdateExpression: "SET #C = :c",
        };
        try {
            await dynamodb.updateItem(paramsUpdate).promise();
        } catch(error){
            return {
                statusCode : 500,
                body : error
            }
        }
    }
    const response = {
        statusCode: 200,
        body: 'Tarjeta creada',
    };
    return response;
}