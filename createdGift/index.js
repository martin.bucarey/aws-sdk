const ddb = require('aws-sdk/clients/dynamodb');
const dynamodb = new ddb({region: 'us-east-1'});
const moment = require('moment');


 exports.handler = async (event)=>{
     const sqs = event.Records.map((record) => record.body);
     for (const item of sqs) {
        const message = JSON.parse(item);
        const body = JSON.parse(message.Message);
        const birthMonth = moment(body.birth).format('M');
        const birthDay = moment(body.birth).format('D');
        let giftBirthday ='';
        if( birthMonth >=1 && birthMonth <= 3){
            if( birthDay >= 21 && birthMonth===3){
                giftBirthday = 'Buzo';
            } else {
                giftBirthday = 'Remera';
            }
        } else if( birthMonth >= 3 && birthMonth <=6){
            if( birthDay >= 21 && birthMonth===6){
                giftBirthday = 'Sweater';
            } else {
                giftBirthday = 'Buzo';
            }
        } else if ( birthMonth >= 6 && birthMonth <=9) {
             if( birthDay >= 21 && birthMonth===9){
                giftBirthday = 'Camisa';
            } else {
                giftBirthday = 'Sweater';
            }
        } else if( birthMonth >= 9 && birthMonth <= 12 ) {
            if( birthDay >= 21 && birthMonth===12){
                giftBirthday = 'Remera';
            } else {
                giftBirthday = 'Camisa';
            }
        }
        const paramsUpdate = {
            ExpressionAttributeNames: {
                "#G": "Gift",
            },
            ExpressionAttributeValues: {
                ":g":{
                      S: giftBirthday,
                }
              }, 
               Key: {
                "dni": {
                  S: body.dni
                 }
               }, 
               ReturnValues: "ALL_NEW",
                TableName: "clients",
                UpdateExpression: "SET #G = :g",
        };
        try {
            await dynamodb.updateItem(paramsUpdate).promise();
        } catch(error){
            return {
                statusCode : 500,
                body: error
            }
        }
        
     }
    const response = {
        statusCode: 200,
        body: 'Regalo creado',
    };
    return response;
 }