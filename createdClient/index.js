const aws = require('aws-sdk');
const sns = new aws.SNS();
const moment = require('moment');
const ddb = require('aws-sdk/clients/dynamodb');
const dynamodb = new ddb({region: 'us-east-1'});

const enabledClient = (birth) => {
    const today = moment();
    const age= today.diff(moment(birth),'years');
    return age >= 18 && age <= 56 ?  true : false;
}

 exports.handler = async (event)=>{
    const body = event;
    let dbParams = {
        Item: {
         "dni": {
           S: body.dni
          }, 
         "name": {
           S: body.name
          }, 
         "lastName": {
           S: body.lastName
          },
          "birth": {
            S: body.birth
          }
        }, 
        ReturnConsumedCapacity: "TOTAL", 
        TableName: "clients"
    };
    let snsParams = {
      Message: JSON.stringify(body),
      TopicArn : 'arn:aws:sns:us-east-1:609844146333:martin-bucarey-clientCreatedNotifiation'
    }
    const { birth } = event;
    if( enabledClient(birth) ){
        try{
            await dynamodb.putItem(dbParams).promise();
            await sns.publish(snsParams).promise();
        } catch(error){
            return {
                statusCode: 500,
                body: error
            }
        }
     } else {
         return {
             statusCode: 400,
             body: 'El cliente no cumple con las restricciones de edad'
         }
     }
    const response = {
        statusCode: 200,
        body: 'Cliente creado',
    };
    return response;
 }